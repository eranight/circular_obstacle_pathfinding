#include "ObstacleModel.h"

using namespace circular_obstacle_pathfinding;

ObstacleModel::ObstacleModel(ObstacleModel&& model) :
	x(model.x),
	y(model.y),
	radius(model.radius),
	endpoint(model.endpoint),
	vertices(std::move(model.vertices)),
	huggingEdges(std::move(model.huggingEdges)),
	intersections(std::move(model.intersections))
{
}

ObstacleModel& ObstacleModel::operator=(ObstacleModel&& model)
{
	x = model.x;
	y = model.y;
	radius = model.radius;
	endpoint = model.endpoint;
	vertices = std::move(model.vertices);
	huggingEdges = std::move(model.huggingEdges);
	intersections = std::move(model.intersections);
	return *this;
}
