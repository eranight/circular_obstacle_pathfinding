#include "Forest.h"
#include "Util.h"
#include "GraphModelBuilder.h"

using namespace std;
using namespace circular_obstacle_pathfinding;

const Obstacle* Forest::getObstacle(uint32_t id) {
	auto iter = obstacles.find(id);
	if (iter == obstacles.end()) {
		return nullptr;
	}
	return &iter->second;
}

vector<Obstacle> Forest::getObstacles() {
	vector<Obstacle> retObstacles(obstacles.size());
	for (auto iter : obstacles) {
		retObstacles.push_back(iter.second);
	}
	return retObstacles;
}

uint32_t Forest::addObstacle(float x, float y, float radius) {
	uint32_t id = idGenerator.generate();
	obstacles[id] = Obstacle(x, y, radius);
	return id;
}

bool Forest::removeObstacle(uint32_t id) {
	return obstacles.erase(id) == 1;
}

bool Forest::changeObstaclePosition(uint32_t id, float x, float y) {
	auto iter = obstacles.find(id);
	if (iter == obstacles.end()) {
		return false;
	}
	iter->second.setX(x);
	iter->second.setY(y);
	return true;
}

bool Forest::changeObstacleRadius(uint32_t id, float radius) {
	auto iter = obstacles.find(id);
	if (iter == obstacles.end()) {
		return false;
	}
	iter->second.setRadius(radius);
	return true;
}

std::shared_ptr<Graph> Forest::getGraph(float additionalWidth)
{
	std::vector<ObstacleModel> obstacleModels;
	for (auto& kv : obstacles) {
		auto& obstacle = kv.second;
		obstacleModels.push_back(ObstacleModel(obstacle.getX(), obstacle.getY(), obstacle.getRadius(), false));
	}
	auto gb = GraphModelBuilder(obstacleModels, additionalWidth);
	auto& model = gb.build();
	return std::make_shared<Graph>(model);
}
