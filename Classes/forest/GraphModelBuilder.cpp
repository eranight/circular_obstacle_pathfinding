#include <algorithm>

#include "GraphModelBuilder.h"
#include "Graph.h"
#include "Util.h"

using namespace circular_obstacle_pathfinding;

static size_t FIRST_INTERNAL_EDGE = 0;
static size_t SECOND_INTERNAL_EDGE = 1;
static size_t FIRST_EXTERNAL_EDGE = 2;
static size_t SECOND_EXTERNAL_EDGE = 3;
static size_t EDGES_NUM = 4;

GraphModelBuilder::GraphModelBuilder(std::vector<ObstacleModel> obstacles, float additionalWidth)
{
	graphModel = std::make_shared<GraphModel>();
	auto& idGenerator = graphModel->obstacleIdGenerator;
	for (auto& obstacle : obstacles) {
		float radius = obstacle.radius;
		if (radius > FLT_EPSILON) {
			radius += additionalWidth;
			if (radius > 0.0f) {
				obstacle.radius = radius;
			}
		}
		graphModel->obstacles[idGenerator.generate()] = obstacle;
	}
}

uint32_t GraphModelBuilder::addEndpoint(float x, float y)
{
	uint32_t id = graphModel->obstacleIdGenerator.generate();
	graphModel->obstacles[id] = ObstacleModel(x, y, 0.0f, true);
	auto& obstacle = graphModel->obstacles[id];
	addEndpointToGraph(id, obstacle);
	return id;
}

void GraphModelBuilder::removeEndpoint(uint32_t endpointId)
{
	removeEndpointFromGraph(endpointId);
}

std::shared_ptr<GraphModel> GraphModelBuilder::build()
{
	if (firstBuild) {
		excludeInnerObstacles();
		buildSurfingEdges();
		buildHuggingEdges();
		firstBuild = false;
	}
	return graphModel;
}

void GraphModelBuilder::excludeInnerObstacles()
{
	auto& obstacles = graphModel->obstacles;
	std::set<uint32_t> skippedObstacles;
	for (auto firstIter = obstacles.begin(); firstIter != prev(obstacles.end()); ++firstIter) {
		if (firstIter->second.endpoint) continue;
		for (auto secondIter = next(firstIter); secondIter != obstacles.end(); ++secondIter) {
			if (secondIter->second.endpoint) continue;
			checkContainAndExclude(firstIter->first, firstIter->second, secondIter->first, secondIter->second, skippedObstacles);
		}
	}
	for (auto id : skippedObstacles) {
		obstacles.erase(id);
	}
}

void GraphModelBuilder::checkContainAndExclude(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle, std::set<uint32_t>& destSkippedObstacles)
{
	if (Util::contain(sourceObstacle.x, sourceObstacle.y, sourceObstacle.radius, destObstacle.x, destObstacle.y, destObstacle.radius)) {
		destSkippedObstacles.insert(sourceObstacle.radius < destObstacle.radius ? sourceId : destId);
	}
}

//*********** ALL ABOUT ENDPOINTS *********************
#pragma region Endpoints

void GraphModelBuilder::removeEndpointFromGraph(uint32_t endpointId)
{
	auto& endpointObstacle = graphModel->obstacles[endpointId];
	for (uint32_t vertexId : endpointObstacle.vertices) { // 0 or 1 for endpoint
		auto& vertex = graphModel->vertices[vertexId];
		std::set<uint32_t> edgeSet = vertex.edges;
		for (uint32_t edgeId : edgeSet) {
			auto& edge = graphModel->edges[edgeId];
			uint32_t toVertexId = edge.getNot(vertexId);
			removeSurfingEdge(edgeId, edge);
			checkAndRemoveOnlyHuggingEdgeVertex(toVertexId);
		}
		graphModel->vertices.erase(vertexId);
	}
	graphModel->obstacles.erase(endpointId);
	graphModel->endpoints.erase(endpointId);
}

void GraphModelBuilder::removeSurfingEdge(uint32_t edgeId, EdgeModel& edge)
{
	graphModel->vertices[edge.vertices.first].edges.erase(edgeId);
	graphModel->vertices[edge.vertices.second].edges.erase(edgeId);
	graphModel->edges.erase(edgeId);
}

void GraphModelBuilder::checkAndRemoveOnlyHuggingEdgeVertex(uint32_t vertexId)
{
	auto& vertex = graphModel->vertices[vertexId];
	if (!vertex.edges.empty() && checkOnlyHuggingEdgesVertex(vertex)) {
		uint32_t obstacleId = graphModel->edges.at(*vertex.edges.begin()).obstacle;
		auto& obstacle = graphModel->obstacles.at(obstacleId);
		if (vertex.edges.size() == 2) {
			auto& edge1 = graphModel->edges[*vertex.edges.begin()];
			auto& edge2 = graphModel->edges[*std::next(vertex.edges.begin())];
			uint32_t from = edge1.getNot(vertexId);
			uint32_t to = edge2.getNot(vertexId);
			if (from == edge1.vertices.second) {
				std::swap(from, to);
			}
			float weight = edge1.weight + edge2.weight;
			addHuggingEdgeToGraph(from, to, weight, obstacleId, obstacle);
		}
		while (!vertex.edges.empty()) {
			removeHuggingEdge(obstacle, *vertex.edges.begin());
		}
		obstacle.vertices.erase(vertexId);
		graphModel->vertices.erase(vertexId);
	}
}

bool GraphModelBuilder::checkOnlyHuggingEdgesVertex(VertexModel& vertex)
{
	for (uint32_t edgeId : vertex.edges) {
		if (!graphModel->edges[edgeId].hugging) {
			return false;
		}
	}
	return true;
}

void GraphModelBuilder::removeHuggingEdge(ObstacleModel& obstacle, uint32_t edgeId)
{
	obstacle.huggingEdges.erase(edgeId);
	auto& edge = graphModel->edges[edgeId];
	graphModel->vertices[edge.vertices.first].edges.erase(edgeId);
	graphModel->vertices[edge.vertices.second].edges.erase(edgeId);
	graphModel->edges.erase(edgeId);
}


void GraphModelBuilder::addEndpointToGraph(uint32_t endpointId, ObstacleModel& obstacle)
{
	auto& obstacles = graphModel->obstacles;
	graphModel->endpoints[endpointId] = std::set<uint32_t>();
	for (auto& keyValue : obstacles) {
		if (keyValue.first == endpointId || keyValue.second.endpoint) continue;
		if (Util::contain(keyValue.second.x, keyValue.second.y, keyValue.second.radius, obstacle.x, obstacle.y)) {
			graphModel->endpoints[endpointId].insert(keyValue.first);
		}
	}
	for (auto& keyValue : obstacles) {
		buildSurfingEdgesBetween(endpointId, obstacle, keyValue.first, keyValue.second);
		if (edgeValids[FIRST_EXTERNAL_EDGE] || edgeValids[SECOND_EXTERNAL_EDGE]) {
			for (uint32_t edgeId : keyValue.second.huggingEdges) {
				auto& edge = graphModel->edges[edgeId];
				graphModel->vertices[edge.vertices.first].edges.erase(edgeId);
				graphModel->vertices[edge.vertices.second].edges.erase(edgeId);
				graphModel->edges.erase(edgeId);
			}
			keyValue.second.huggingEdges.clear();
			buildHuggingEdgesOn(keyValue.first, keyValue.second);
		}
		clearCachedEdges();
	}
}

#pragma endregion

//*********** ALL ABOUT SURFING EDGES *********************
#pragma region SurfingEdges

void GraphModelBuilder::buildSurfingEdges() {
	auto& obstacles = graphModel->obstacles;
	for (auto firstIter = obstacles.begin(); firstIter != prev(obstacles.end()); ++firstIter) {
		for (auto secondIter = next(firstIter); secondIter != obstacles.end(); ++secondIter) {
			buildSurfingEdgesBetween(firstIter->first, firstIter->second, secondIter->first, secondIter->second);
			clearCachedEdges();
		}
	}
}

void GraphModelBuilder::buildSurfingEdgesFor(uint32_t id, ObstacleModel& obstacle)
{
	auto& obstacles = graphModel->obstacles;
	for (auto& keyValue : obstacles) {
		buildSurfingEdgesBetween(id, obstacle, keyValue.first, keyValue.second);
	}
}

void GraphModelBuilder::buildSurfingEdgesBetween(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle)
{
	calculateSurfingEdgesBetween(sourceId, sourceObstacle, destId, destObstacle);
	excludeBadSurfingEdgesBetween(sourceId, sourceObstacle, destId, destObstacle);
	buildValidSurfingEdgesBetween(sourceObstacle, destObstacle);
}

void GraphModelBuilder::calculateSurfingEdgesBetween(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle)
{
	if (Util::equals(sourceObstacle.x, sourceObstacle.y, sourceObstacle.radius, destObstacle.x, destObstacle.y, destObstacle.radius)) {
		return;
	}
	calculateInternalSurfingEdgesIfNeeded(sourceId, sourceObstacle, destId, destObstacle);
	calculateExternalSurfingEdges(sourceId, sourceObstacle, destId, destObstacle);
}

void GraphModelBuilder::calculateInternalSurfingEdgesIfNeeded(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle)
{
	if (sourceObstacle.endpoint || destObstacle.endpoint) {
		edgeValids[FIRST_INTERNAL_EDGE] = false;
		edgeValids[SECOND_INTERNAL_EDGE] = false;
		return;
	}
	float distance = Util::distance(sourceObstacle.getX(), sourceObstacle.getY(), destObstacle.getX(), destObstacle.getY());
	int overlap = Util::overlap(sourceObstacle.getRadius(), destObstacle.getRadius(), distance);
	if (overlap == 0) {
		return;
	}
	else if (overlap == 1) {

		float theta = Util::theta(sourceObstacle.radius, destObstacle.radius, distance);
		float facing = Util::facing(sourceObstacle.x, sourceObstacle.y, destObstacle.x, destObstacle.y);
		sourceObstacle.intersections.insert(Util::convertAngleTo2Pi(facing + theta));
		if (abs(theta) > FLT_EPSILON) {
			sourceObstacle.intersections.insert(Util::convertAngleTo2Pi(facing - theta));
		}

		theta = Util::theta(destObstacle.radius, sourceObstacle.radius, distance);
		facing = Util::facing(destObstacle.x, destObstacle.y, sourceObstacle.x, sourceObstacle.y);
		destObstacle.intersections.insert(Util::convertAngleTo2Pi(facing + theta));
		if (abs(theta) > FLT_EPSILON) {
			destObstacle.intersections.insert(Util::convertAngleTo2Pi(facing - theta));
		}

		edgeValids[FIRST_INTERNAL_EDGE] = false;
		edgeValids[SECOND_INTERNAL_EDGE] = false;
		return;
	}
	else if (sourceObstacle.getRadius() < FLT_EPSILON || destObstacle.getRadius() < FLT_EPSILON) {
		edgeValids[FIRST_INTERNAL_EDGE] = false;
		edgeValids[SECOND_INTERNAL_EDGE] = false;
		return;
	}
	auto internalBitangents = InternalBitangents(sourceObstacle, destObstacle);
	calculateSurfingEdges(FIRST_INTERNAL_EDGE, SECOND_INTERNAL_EDGE, internalBitangents);
}

void GraphModelBuilder::calculateExternalSurfingEdges(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle)
{
	if (sourceObstacle.endpoint || destObstacle.endpoint) {
		uint32_t endpointId = sourceObstacle.endpoint ? sourceId : destId;
		uint32_t nonEndpointId = sourceObstacle.endpoint ? destId : sourceId;
		auto& containingObstacles = graphModel->endpoints[endpointId];
		if (containingObstacles.find(nonEndpointId) != containingObstacles.end()) {
			return;
		}
	}
	auto externalBitangents = ExternalBitangents(sourceObstacle, destObstacle);
	if (sourceObstacle.endpoint && destObstacle.endpoint) {
		calculateSingleSurfingEdge(FIRST_EXTERNAL_EDGE, externalBitangents);
		edgeValids[SECOND_EXTERNAL_EDGE] = false;
	}
	else {
		calculateSurfingEdges(FIRST_EXTERNAL_EDGE, SECOND_EXTERNAL_EDGE, externalBitangents);
	}
}

void GraphModelBuilder::excludeBadSurfingEdgesBetween(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle)
{
	auto& obstacles = graphModel->obstacles;
	for (auto& keyValue : obstacles) {
		ObstacleModel& obstacle = keyValue.second;
		if (keyValue.first == sourceId || keyValue.first == destId) continue;
		if (ignoreIntersection(sourceId, sourceObstacle, destId, destObstacle, keyValue.first)) continue;
		int intersectionCounter = 0;
		for (int index = 0; index < edges.size(); ++index) {
			if (edgeValids[index]) {
				if (intersects(index, obstacle)) {
					edgeValids[index] = false;
					++intersectionCounter;
				}
			}
			else {
				++intersectionCounter;
			}
		}
		if (intersectionCounter == EDGES_NUM) {
			break;
		}
	}
}

void GraphModelBuilder::buildValidSurfingEdgesBetween(ObstacleModel& sourceObstacle, ObstacleModel& destObstacle)
{
	for (int index = 0; index < edges.size(); ++index) {
		if (edgeValids[index]) {
			processSurfingEdge(sourceObstacle, destObstacle, index);
		}
	}
}

void GraphModelBuilder::calculateSurfingEdges(size_t firstEdgeCacheId, size_t secondEdgeCacheId, Bitangents& bitangents) {
	calculateSingleSurfingEdge(firstEdgeCacheId, bitangents);

	auto& edge = edges[secondEdgeCacheId];
	auto& fromVertex = vertices[secondEdgeCacheId * 2];
	auto& toVertex = vertices[secondEdgeCacheId * 2 + 1];
	bitangents.secondSegment(&fromVertex.x, &fromVertex.y, &toVertex.x, &toVertex.y);
	edge.weight = Util::distance(fromVertex.x, fromVertex.y, toVertex.x, toVertex.y);
	edgeValids[secondEdgeCacheId] = true;
}

void GraphModelBuilder::calculateSingleSurfingEdge(size_t edgeCacheId, Bitangents& bitangents)
{
	auto& edge = edges[edgeCacheId];
	auto& fromVertex = vertices[edgeCacheId * 2];
	auto& toVertex = vertices[edgeCacheId * 2 + 1];
	bitangents.firstSegment(&fromVertex.x, &fromVertex.y, &toVertex.x, &toVertex.y);
	edge.weight = Util::distance(fromVertex.x, fromVertex.y, toVertex.x, toVertex.y);
	edgeValids[edgeCacheId] = true;
}

bool GraphModelBuilder::ignoreIntersection(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle, uint32_t id)
{
	auto iter = graphModel->endpoints.find(sourceId);
	auto secondIter = graphModel->endpoints.find(destId);
	return (iter != graphModel->endpoints.end() && iter->second.find(id) != iter->second.end()) ||
		(secondIter != graphModel->endpoints.end() && secondIter->second.find(id) != secondIter->second.end());
}

bool GraphModelBuilder::intersects(size_t edgeCacheId, ObstacleModel& checkedObstacle) {
	auto& edge = edges[edgeCacheId];
	auto& fromVertex = vertices[edgeCacheId * 2];
	auto& toVertex = vertices[edgeCacheId * 2 + 1];
	float fraction = Util::fraction(
		fromVertex.x, fromVertex.y,
		toVertex.x, toVertex.y,
		checkedObstacle.getX(), checkedObstacle.getY(),
		edge.weight);
	float distToLine = Util::distanceToLine(
		fromVertex.x, fromVertex.y,
		toVertex.x, toVertex.y,
		checkedObstacle.getX(), checkedObstacle.getY(),
		Util::clamp(fraction, 0.0f, 1.0f));
	if (abs(distToLine - checkedObstacle.getRadius()) < FLT_EPSILON) {
		return false;
	}
	return distToLine < checkedObstacle.getRadius();
}

void GraphModelBuilder::processSurfingEdge(ObstacleModel& sourceObstacle, ObstacleModel& destObstacle, size_t edgeCacheId) {
	uint32_t fromId = mergeOrAddVertices(sourceObstacle, vertices[edgeCacheId * 2]);
	uint32_t toId = mergeOrAddVertices(destObstacle, vertices[edgeCacheId * 2 + 1]);
	uint32_t edgeId = graphModel->edgesIdGenerator.generate();
	auto& edge = (graphModel->edges[edgeId] = EdgeModel(fromId, toId, edges[edgeCacheId].weight));
	graphModel->vertices[fromId].edges.insert(edgeId);
	graphModel->vertices[toId].edges.insert(edgeId);

	if (edgeCacheId == FIRST_INTERNAL_EDGE) {
		// points C & F of internal bitangent have a clockwise outgoing direction
		edge.outgoingDirections.first = EdgeDirection::clockwise;
		edge.outgoingDirections.second = EdgeDirection::clockwise;
	}
	else if (edgeCacheId == SECOND_INTERNAL_EDGE) {
		// points D & E of internal bitangent have a counterclockwise outgoing direction
		edge.outgoingDirections.first = EdgeDirection::counterclockwise;
		edge.outgoingDirections.second = EdgeDirection::counterclockwise;
	}
	else if (edgeCacheId == FIRST_EXTERNAL_EDGE) {
		// point C of external bitangent has a clockwise outgoint direction
		edge.outgoingDirections.first = sourceObstacle.endpoint ? EdgeDirection::any : EdgeDirection::clockwise;
		// point F of external bitangent has a counterclockwise outgoint direction
		edge.outgoingDirections.second = destObstacle.endpoint ? EdgeDirection::any : EdgeDirection::counterclockwise;
	}
	else { // if (edgeCacheId == SECOND_EXTERNAL_EDGE) {
		// point D of external bitangent has a counterclockwise outgoint direction
		edge.outgoingDirections.first = sourceObstacle.endpoint ? EdgeDirection::any : EdgeDirection::counterclockwise;
		// point E of external bitangent has a clockwise outgoint direction
		edge.outgoingDirections.second = destObstacle.endpoint ? EdgeDirection::any : EdgeDirection::clockwise;
	}
}

uint32_t GraphModelBuilder::mergeOrAddVertices(ObstacleModel& obstacle, VertexModel& edgeEndpoint)
{
	uint32_t id;
	float x = edgeEndpoint.x;
	float y = edgeEndpoint.y;
	bool merged = findVertex(obstacle, x, y, id);
	if (!merged) {
		id = graphModel->verticesIdGenerator.generate();
		edgeEndpoint.facing = Util::facing(obstacle.x, obstacle.y, x, y);
		graphModel->vertices[id] = edgeEndpoint;
		obstacle.vertices.insert(id);
	}
	return id;
}

bool GraphModelBuilder::findVertex(ObstacleModel& obstacle, float x, float y, uint32_t& dest)
{
	for (auto& vertexId : obstacle.vertices) {
		auto& vertex = graphModel->vertices[vertexId];
		if (Util::equals(x, y, vertex.x, vertex.y)) {
			dest = vertexId;
			return true;
		}
	}
	return false;
}

void GraphModelBuilder::clearCachedEdges()
{
	for (int index = 0; index < EDGES_NUM; ++index) {
		edgeValids[index] = false;
	}
}

#pragma endregion

//*********** ALL ABOUT HUGGING EDGES *********************
#pragma region HuggingEdges

void GraphModelBuilder::buildHuggingEdges() {
	auto& obstacles = graphModel->obstacles;
	for (auto& keyValue : obstacles) {
		ObstacleModel& obstacle = keyValue.second;
		buildHuggingEdgesOn(keyValue.first, keyValue.second);
	}
}

void GraphModelBuilder::buildHuggingEdgesOn(uint32_t obstacleId, ObstacleModel& obstacle)
{
	if (ignoreCalculateHuggingEdges(obstacle)) return;
	calculateAnglesOnObstacle(obstacle);
	calculateAndBuildHuggingEdges(obstacleId, obstacle);
	angles.clear();
}

bool GraphModelBuilder::ignoreCalculateHuggingEdges(ObstacleModel& obstacle)
{
	return obstacle.vertices.empty() || obstacle.radius < FLT_EPSILON;
}

void GraphModelBuilder::calculateAnglesOnObstacle(ObstacleModel& obstacle)
{
	for (float intersectionAngle : obstacle.intersections) {
		angles.push_back(std::make_tuple(intersectionAngle, false, -1));
	}
	for (uint32_t vertexId : obstacle.vertices) {
		auto& vertex = graphModel->vertices[vertexId];
		angles.push_back(std::make_tuple(Util::convertAngleTo2Pi(vertex.facing), true, vertexId));
	}
	std::sort(angles.begin(), angles.end(), [](auto& tuple1, auto& tuple2) { return std::get<0>(tuple1) > std::get<0>(tuple2); });
}

void GraphModelBuilder::calculateAndBuildHuggingEdges(uint32_t obstacleId, ObstacleModel& obstacle)
{
	for (auto iter = angles.begin(); iter != angles.end(); ++iter) {
		auto nextIter = iter + 1;
		if (nextIter == angles.end()) {
			nextIter = angles.begin();
		}
		if (std::get<1>(*iter) && std::get<1>(*nextIter)) {
			float weight = calculateHuggingEdgeWeight(std::get<0>(*iter), std::get<0>(*nextIter), obstacle.radius);
			addHuggingEdgeToGraph(std::get<2>(*iter), std::get<2>(*nextIter), weight, obstacleId, obstacle);
		}
	}
}

float GraphModelBuilder::calculateHuggingEdgeWeight(float angleFrom, float angleTo, float radius)
{
	return (angleFrom > angleTo || abs(angleFrom - angleTo) < FLT_EPSILON ?
		abs(angleFrom - angleTo) :
		angleFrom + (2 * M_PI - angleTo))
		* radius;
}

void GraphModelBuilder::addHuggingEdgeToGraph(uint32_t from, uint32_t to, float weight, uint32_t obstacleId, ObstacleModel& obstacle) {
	uint32_t edgeId = graphModel->edgesIdGenerator.generate();
	auto& edge = (graphModel->edges[edgeId] = EdgeModel(from, to, weight, true, obstacleId));
	// for hugging edges we always moving clockwise from first to second
	edge.outgoingDirections.first = EdgeDirection::clockwise;
	edge.outgoingDirections.second = EdgeDirection::counterclockwise;
	obstacle.huggingEdges.insert(edgeId);
	graphModel->vertices[from].edges.insert(edgeId);
	graphModel->vertices[to].edges.insert(edgeId);
}

#pragma endregion