#pragma once

#include <map>
#include <vector>
#include <memory>
#include <tuple>
#include "GraphModel.h"
#include "ObstacleModel.h"
#include "VertexModel.h"
#include "EdgeModel.h"
#include "Bitangents.h"

namespace circular_obstacle_pathfinding {

	class GraphModelBuilder {
	public:
		GraphModelBuilder(std::vector<ObstacleModel> obstacles, float additionalWidth);
		GraphModelBuilder(std::shared_ptr<GraphModel> graphModel) : graphModel(graphModel), firstBuild(false) {}
	public:
		uint32_t addEndpoint(float x, float y);
		void removeEndpoint(uint32_t endpointId);
		std::shared_ptr<GraphModel> build();
	private:
		void excludeInnerObstacles();
		void checkContainAndExclude(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle, std::set<uint32_t>& destSkippedObstacles);

		void removeEndpointFromGraph(uint32_t endpointId);
		void removeSurfingEdge(uint32_t edgeId, EdgeModel& edge);
		void checkAndRemoveOnlyHuggingEdgeVertex(uint32_t vertexId);
		bool checkOnlyHuggingEdgesVertex(VertexModel& vertex);
		void removeHuggingEdge(ObstacleModel& obstacle, uint32_t edgeId);
		void addEndpointToGraph(uint32_t endpointId, ObstacleModel& obstacle);

		void buildSurfingEdges();
		void buildSurfingEdgesBetween(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle);
		void buildSurfingEdgesFor(uint32_t id, ObstacleModel& obstacle);
		void calculateSurfingEdgesBetween(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle);
		void calculateInternalSurfingEdgesIfNeeded(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle);
		void calculateExternalSurfingEdges(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle);
		void excludeBadSurfingEdgesBetween(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle);
		void buildValidSurfingEdgesBetween(ObstacleModel& sourceObstacle, ObstacleModel& destObstacle);
		void calculateSurfingEdges(size_t firstEdgeCacheId, size_t secondEdgeCacheId, Bitangents& bitangents);
		void calculateSingleSurfingEdge(size_t edgeCacheId, Bitangents& bitangents);
		bool ignoreIntersection(uint32_t sourceId, ObstacleModel& sourceObstacle, uint32_t destId, ObstacleModel& destObstacle, uint32_t id);
		bool intersects(size_t edgeCacheId, ObstacleModel& checkedObstacle);
		void processSurfingEdge(ObstacleModel& sourceObstacle, ObstacleModel& destObstacle, size_t edgeCacheId);
		uint32_t mergeOrAddVertices(ObstacleModel& obstacle, VertexModel& edgeEndpoint);
		bool findVertex(ObstacleModel& obstacle, float x, float y, uint32_t& dest);
		void clearCachedEdges();

		void buildHuggingEdges();
		void buildHuggingEdgesOn(uint32_t obstacleId, ObstacleModel& obstacleBuilder);
		bool ignoreCalculateHuggingEdges(ObstacleModel& obstacle);
		void calculateAnglesOnObstacle(ObstacleModel& obstacle);
		void calculateAndBuildHuggingEdges(uint32_t obstacleId, ObstacleModel& obstacle);
		float calculateHuggingEdgeWeight(float angleFrom, float angleTo, float radius);
		void addHuggingEdgeToGraph(uint32_t from, uint32_t to, float weight, uint32_t obstacleId, ObstacleModel& obstacle);

	private:
		std::shared_ptr<GraphModel> graphModel;
		bool firstBuild = true;
		// cache
		std::vector<EdgeModel> edges = std::vector<EdgeModel>(4);
		std::vector<VertexModel> vertices = std::vector<VertexModel>(8);
		std::vector<bool> edgeValids = { false, false, false, false };
		std::vector<std::tuple<float, bool, uint32_t>> angles; // angle value, true if vertex false if intersection, vertex id if second is true
	};

}