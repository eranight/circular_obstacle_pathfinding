#pragma once

#include <stdint.h>
#include <tuple>

namespace circular_obstacle_pathfinding {

	enum EdgeDirection
	{
		any = 0,         // use for endpoints
		clockwise,
		counterclockwise
	};

	class EdgeModel {
	public:
		EdgeModel() {}
		EdgeModel(uint32_t from, uint32_t to, float weight, bool hugging = false, uint32_t obstacle = -1) :
			vertices(std::make_pair(from, to)), weight(weight), hugging(hugging), obstacle(obstacle)
		{}
	public:
		uint32_t getNot(uint32_t id) { return vertices.first == id ? vertices.second : vertices.first; }
		EdgeDirection getDirection(uint32_t id) { return vertices.first == id ? outgoingDirections.first : outgoingDirections.second; }
	public:
		std::pair<uint32_t, uint32_t> vertices; // for using with GraphModel
		std::pair<EdgeDirection, EdgeDirection> outgoingDirections; // holds directions for vertices
		float weight;
		bool hugging;
		uint32_t obstacle; // set if hugging == true, for using with GraphModel
	};
}