#include "Util.h"
#include <cmath>
#include <float.h>

#define TWO_M_PI 2 * M_PI

using namespace circular_obstacle_pathfinding;

float Util::distance(float x1, float y1, float x2, float y2) {
	return sqrtf(distanceSq(x1, y1, x2, y2));
}

float Util::distanceSq(float x1, float y1, float x2, float y2)
{
	float x = x2 - x1;
	float y = y2 - y1;
	return x * x + y * y;
}

bool Util::equals(float x1, float y1, float x2, float y2)
{
	if (x1 - FLT_EPSILON <= x2 && x2 <= x1 + FLT_EPSILON)
		if (y1 - FLT_EPSILON <= y2 && y2 <= y1 + FLT_EPSILON)
			return true;
	return false;
}

bool Util::equals(float x1, float y1, float radiusA, float x2, float y2, float radiusB)
{
	return abs(radiusA - radiusB) < FLT_EPSILON && equals(x1, y1, x2, y2);
}

int Util::overlap(float radiusA, float radiusB, float distance) {
	if (distance < FLT_EPSILON || distance < abs(radiusA - radiusB) + FLT_EPSILON) {
		return 0;
	}
	else if (distance > radiusA + radiusB + FLT_EPSILON) {
		return 2;
	}
	else {
		return 1;
	}
}

bool Util::contain(float radiusA, float radiusB, float distance) {
	float diffRad = abs(radiusA - radiusB);
	return distance < FLT_EPSILON && diffRad > FLT_EPSILON || distance < diffRad + FLT_EPSILON;
}

bool Util::contain(float x1, float y1, float radius, float x2, float y2)
{
	return distanceSq(x1, y1, x2, y2) < radius * radius;
}

bool Util::contain(float x1, float y1, float radius1, float x2, float y2, float radius2)
{
	float diffRad = abs(radius1 - radius2);
	return distanceSq(x1, y1, x2, y2) < diffRad * diffRad;
}

float Util::theta(float radiusA, float radiusB, float distance)
{
	float a = (radiusA * radiusA - radiusB * radiusB + distance * distance) / (2.0f * distance);
	float theta = acosf(a / radiusA);
	if (theta < FLT_EPSILON) return 0.0f;
	return theta;
}

float Util::fraction(float x1, float y1, float x2, float y2, float x3, float y3, float distance) {
	return ((x3 - x1) * (x2 - x1) + (y3 - y1) * (y2 - y1)) / (distance * distance);
}

float Util::fraction(float x1, float y1, float x2, float y2, float x3, float y3)
{
	float distSq = distanceSq(x1, y1, x2, y2);
	return ((x3 - x1) * (x2 - x1) + (y3 - y1) * (y2 - y1)) / distSq;
}

float Util::distanceToLine(float x1, float y1, float x2, float y2, float x3, float y3, float fraction) {
	float x = x1 + fraction * (x2 - x1);
	float y = y1 + fraction * (y2 - y1);
	float absX = abs(x3 - x);
	float absY = abs(y3 - y);
	return sqrtf(absX * absX + absY * absY);
}

float Util::clamp(float n, float lower, float upper) {
	return fmaxf(lower, fminf(n, upper));
}

float Util::facing(float x1, float y1, float x2, float y2) {
	float x = x2 - x1;
	float y = y2 - y1;
	return atan2f(y, x);
}

void Util::pointOnCircle(float x, float y, float radius, float angle, float* destX, float* destY) {
	float polarX = radius * cosf(angle);
	float polarY = radius * sinf(angle);
	*destX = x + polarX;
	*destY = y + polarY;
}

float Util::convertAngleTo2Pi(float angle) {
	return angle < 0.0f ? (angle + TWO_M_PI) : angle;
}

float Util::convertAngleToPi(float angle)
{
	return abs(angle - M_PI) > FLT_EPSILON ? (angle - (TWO_M_PI * (int)(angle / TWO_M_PI))) : angle;
}
