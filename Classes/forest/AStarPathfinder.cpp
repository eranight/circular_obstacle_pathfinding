#include "AStarPathfinder.h"
#include "GraphModel.h"
#include "Util.h"

using namespace std;
using namespace circular_obstacle_pathfinding;

static EdgeDirection getOpposite(EdgeDirection dir);

Path AStarPathfinder::findPath(uint32_t start, uint32_t goal)
{
	currentNodeOnPrecedingAndEdge.clear();
	observedEdges.clear();
	gScore.clear();
	fScore.clear();
	VertexModel startVertex = model->vertices.at(start);
	this->goal = goal;
	goalVertex = model->vertices.at(goal);
	startGoalDistance = Util::distanceSq(startVertex.x, startVertex.y, goalVertex.x, goalVertex.y);
	set<pair<uint32_t, EdgeDirection>> openNodes;
	openNodes.insert({start, EdgeDirection::any});
	fScore[{ start, EdgeDirection::any }] = 0.0f;
	gScore[{ start, EdgeDirection::any }] = 0.0f;
	Path path;
	while (!openNodes.empty()) {
		pair<uint32_t, EdgeDirection> currentNode = getNextNode(openNodes);
		if (currentNode.first == goal) {
			reconstructPath(currentNode, path);
			return path;
		}
		openNodes.erase(currentNode);
		getNeighbors(currentNode, model->vertices[currentNode.first], openNodes);
	}
	return Path();
}

void AStarPathfinder::reconstructPath(KeyPair currentNodeId, Path& destPath)
{
	destPath.parts.push_back(make_pair(currentNodeId.first, -1));
	auto iter = currentNodeOnPrecedingAndEdge.find(currentNodeId);
	while (iter != currentNodeOnPrecedingAndEdge.end()) {
		currentNodeId = iter->second.first;
		uint32_t edgeId = iter->second.second;
		destPath.parts.insert(destPath.parts.begin(), make_pair(currentNodeId.first, edgeId));
		iter = currentNodeOnPrecedingAndEdge.find(currentNodeId);
	}
}

pair<uint32_t, EdgeDirection> AStarPathfinder::getNextNode(std::set<pair<uint32_t, EdgeDirection>>& openNodes)
{
	pair<uint32_t, EdgeDirection> result;
	float min = INFINITY;
	for (auto& openedNodePair : openNodes) {
		float checkedMin = getFScore(openedNodePair);
		if (checkedMin < min) {
			min = checkedMin;
			result = openedNodePair;
		}
	}
	if (!currentNodeOnPrecedingAndEdge.empty()) {
		observedEdges.insert(currentNodeOnPrecedingAndEdge.at(result).second);
	}
	return result;
}

void AStarPathfinder::getNeighbors(const KeyPair& nodeId, VertexModel& vertex, std::set<pair<uint32_t, EdgeDirection>>& openNodes)
{
	pair<uint32_t, EdgeDirection> neighbor;
	for (uint32_t edgeId : vertex.edges) {
		auto& edge = model->edges[edgeId];
		if (checkEdgeAndGetNeighbors(nodeId, edge, neighbor)) {
			float score = getGScore(nodeId) + edge.weight;
			if (score < getGScore(neighbor)) {
				currentNodeOnPrecedingAndEdge[neighbor] = make_pair(nodeId, edgeId);
				gScore[neighbor] = score;
				fScore[neighbor] = score + h(neighbor);
				openNodes.insert(neighbor);
			}
		}
	}
}

bool AStarPathfinder::checkEdgeAndGetNeighbors(const KeyPair& nodeId, EdgeModel& edge, KeyPair& dest)
{
	uint32_t destId = edge.getNot(nodeId.first);
	EdgeDirection destDir = edge.getDirection(destId);
	if (nodeId.second == EdgeDirection::any) {
		dest = { destId, getOpposite(destDir) };
		return true;
	}
	EdgeDirection sourceDir = edge.getDirection(nodeId.first);
	if (nodeId.second == sourceDir) {
		dest = { destId, getOpposite(destDir) };
		return true;
	}
	return false;
}

float AStarPathfinder::getFScore(const KeyPair& id)
{
	return getScore(id, fScore);
}

float AStarPathfinder::getGScore(const KeyPair& id)
{
	return getScore(id, gScore);
}

float AStarPathfinder::getScore(const KeyPair& id, std::map<KeyPair, float>& scoreSource)
{
	auto iter = scoreSource.find(id);
	return iter != scoreSource.end() ? iter->second : INFINITY;
}

float AStarPathfinder::h(const KeyPair& id)
{
	VertexModel vertex = model->vertices.at(id.first);
	float dist = Util::distanceSq(vertex.x, vertex.y, goalVertex.x, goalVertex.y);
	float factor = std::sqrtf(dist / startGoalDistance);
	return factor * (factor > 1.0f ? 1200.0f : 0.0f);
	//return factor * 1000.0f;
	//return 0.0f;
}

EdgeDirection getOpposite(EdgeDirection dir)
{
	if (dir == EdgeDirection::any) {
		return dir;
	}
	return dir == EdgeDirection::clockwise ? EdgeDirection::counterclockwise : EdgeDirection::clockwise;
}
