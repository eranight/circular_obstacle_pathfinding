#pragma once

namespace circular_obstacle_pathfinding {

	class Util {
	private:
		Util() {}
	public:
		static float distance(float x1, float y1, float x2, float y2);
		static float distanceSq(float x1, float y1, float x2, float y2);
		static bool equals(float x1, float y1, float x2, float y2);
		static bool equals(float x1, float y1, float radiusA, float x2, float y2, float radiusB);
		static int overlap(float radiusA, float radiusB, float distance);
		static bool contain(float radiusA, float radiusB, float distance);
		static bool contain(float x1, float y1, float radius, float x2, float y2);
		// return true if circle1 contains circle2 or vice versa
		static bool contain(float x1, float y1, float radius1, float x2, float y2, float radius2);
		static float theta(float radiusA, float radiusB, float distance);
		static float fraction(float x1, float y1, float x2, float y2, float x3, float y3, float distance);
		static float fraction(float x1, float y1, float x2, float y2, float x3, float y3);
		static float distanceToLine(float x1, float y1, float x2, float y2, float x3, float y3, float fraction);
		static float clamp(float n, float lower, float upper);
		static float facing(float x1, float y1, float x2, float y2);
		static void pointOnCircle(float x, float y, float radius, float angle, float* destX, float* destY);
		static float convertAngleTo2Pi(float angle);
		static float convertAngleToPi(float angle);
	};

}