#pragma once

#include <vector>
#include <set>
#include <map>
#include <memory>

#include "EdgeModel.h"
#include "VertexModel.h"

namespace circular_obstacle_pathfinding {

	class Path {
	public:
		std::vector<std::pair<uint32_t, uint32_t>> parts; // pair of node id & edge id
	};

	class GraphModel;

	using KeyPair = std::pair<uint32_t, EdgeDirection>;

	class AStarPathfinder {
	public:
		AStarPathfinder(std::shared_ptr<GraphModel> model) : model(model) {}
	public:
		Path findPath(uint32_t start, uint32_t goal);
		std::set<uint32_t>& getAllObservedEdges() { return observedEdges; }
	private:
		void reconstructPath(KeyPair currentNodeId, Path& destPath);
		std::pair<uint32_t, EdgeDirection> getNextNode(std::set<KeyPair>& openNodes);
		void getNeighbors(const KeyPair& nodeId, VertexModel& vertex, std::set<KeyPair>& openNodes);
		bool checkEdgeAndGetNeighbors(const KeyPair& nodeId, EdgeModel& edge, KeyPair& dest);
		float getFScore(const KeyPair& id);
		float getGScore(const KeyPair& id);
		float getScore(const KeyPair& id, std::map<KeyPair, float>& scoreSource);
		float h(const KeyPair& id);
	private:
		std::shared_ptr<GraphModel> model;
		// cache
		uint32_t goal;
		VertexModel goalVertex;
		float startGoalDistance;
		std::map<KeyPair, float> fScore;
		std::map<KeyPair, float> gScore;
		std::map<KeyPair, std::pair<KeyPair, uint32_t>> currentNodeOnPrecedingAndEdge;
		std::set<uint32_t> observedEdges;
	};
}