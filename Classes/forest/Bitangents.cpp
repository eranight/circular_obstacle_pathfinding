#include "Bitangents.h"
#include <math.h>
#include "Util.h"

using namespace circular_obstacle_pathfinding;

Bitangents::Bitangents(const ObstacleModel& obstacleA, const ObstacleModel& obstacleB) : obstacleA(obstacleA), obstacleB(obstacleB), thetaAngle(NAN) {
	facingAB = Util::facing(obstacleA.getX(), obstacleA.getY(), obstacleB.getX(), obstacleB.getY());
}

void Bitangents::firstSegment(float* destX1, float* destY1, float* destX2, float* destY2) {
	c(destX1, destY1);
	f(destX2, destY2);
}

void Bitangents::secondSegment(float* destX1, float* destY1, float* destX2, float* destY2) {
	d(destX1, destY1);
	e(destX2, destY2);
}

void Bitangents::c(float* destX, float* destY) {
	Util::pointOnCircle(obstacleA.getX(), obstacleA.getY(), obstacleA.getRadius(), facingAB + theta(), destX, destY);
}

void Bitangents::d(float* destX, float* destY) {
	Util::pointOnCircle(obstacleA.getX(), obstacleA.getY(), obstacleA.getRadius(), facingAB - theta(), destX, destY);
}

void Bitangents::e(float* destX, float* destY) {
	Util::pointOnCircle(obstacleB.getX(), obstacleB.getY(), obstacleB.getRadius(), facingAB - theta(), destX, destY);
}

void Bitangents::f(float* destX, float* destY) {
	Util::pointOnCircle(obstacleB.getX(), obstacleB.getY(), obstacleB.getRadius(), facingAB + theta(), destX, destY);
}

InternalBitangents::InternalBitangents(const ObstacleModel& obstacleA, const ObstacleModel& obstacleB) : Bitangents(obstacleA, obstacleB) {
	facingBA = Util::facing(obstacleB.getX(), obstacleB.getY(), obstacleA.getX(), obstacleA.getY());
}

float InternalBitangents::theta() {
	if (isnan(thetaAngle)) {
		float distance = Util::distance(obstacleA.getX(), obstacleA.getY(), obstacleB.getX(), obstacleB.getY());
		thetaAngle = acosf((obstacleA.getRadius() + obstacleB.getRadius()) / distance);
	}
	return thetaAngle;
}

void InternalBitangents::e(float* destX, float* destY) {
	Util::pointOnCircle(obstacleB.getX(), obstacleB.getY(), obstacleB.getRadius(), facingBA - theta(), destX, destY);
}

void InternalBitangents::f(float* destX, float* destY) {
	Util::pointOnCircle(obstacleB.getX(), obstacleB.getY(), obstacleB.getRadius(), facingBA + theta(), destX, destY);
}

float ExternalBitangents::theta() {
	if (isnan(thetaAngle)) {
		float distance = Util::distance(obstacleA.getX(), obstacleA.getY(), obstacleB.getX(), obstacleB.getY());
		thetaAngle = acosf((obstacleA.getRadius() - obstacleB.getRadius()) / distance);
	}
	return thetaAngle;
}