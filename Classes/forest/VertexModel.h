#pragma once

#include <cmath>
#include <set>

namespace circular_obstacle_pathfinding {

	class VertexModel {
	public:
		VertexModel() : x(NAN), y(NAN), facing(NAN) {}
		VertexModel(float x, float y, float facing) : x(x), y(y), facing(facing) {}
	public:
		float x;
		float y;
		float facing;             // angle of vector from center of obstacle to vertex
		std::set<uint32_t> edges; // ids for using with GraphModel
	};
}