#include "Graph.h"
#include "GraphModel.h"
#include "GraphModelBuilder.h"

using namespace circular_obstacle_pathfinding;

Graph::Graph(std::shared_ptr<GraphModel> model) :
	model(model)
{
	pathfinder = std::make_shared<AStarPathfinder>(this->model);
}

uint32_t Graph::addEndpoint(float x, float y)
{
	return GraphModelBuilder(model).addEndpoint(x, y);
}

void Graph::removeEndpoint(uint32_t id)
{
	GraphModelBuilder(model).removeEndpoint(id);
}

Path Graph::findPath(uint32_t from, uint32_t to)
{
	auto& fromVertices = model->obstacles.at(from).vertices;
	if (fromVertices.empty()) return Path();
	auto& toVertices = model->obstacles.at(to).vertices;
	if (toVertices.empty()) return Path();
	return pathfinder->findPath(*fromVertices.begin(), *toVertices.begin());
}

std::map<uint32_t, ObstacleModel>& Graph::getObstacles()
{
	return model->obstacles;
}

std::map<uint32_t, VertexModel>& Graph::getVertices()
{
	return model->vertices;
}

std::map<uint32_t, EdgeModel>& Graph::getEdges()
{
	return model->edges;
}

