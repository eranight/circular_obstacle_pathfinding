#pragma once

#include "ObstacleModel.h"

namespace circular_obstacle_pathfinding {

	class Bitangents {
	public:
		Bitangents(const ObstacleModel& obstacleA, const ObstacleModel& obstacleB);
	public:
		void firstSegment(float* destX1, float* destY1, float* destX2, float* destY2);
		void secondSegment(float* destX1, float* destY1, float* destX2, float* destY2);
	protected:
		void c(float* destX, float* destY);
		void d(float* destX, float* destY);
		virtual void e(float* destX, float* destY);
		virtual void f(float* destX, float* destY);
		virtual float theta() = 0;
	protected:
		ObstacleModel obstacleA;
		ObstacleModel obstacleB;
		float thetaAngle;
		float facingAB;
	};

	class InternalBitangents : public Bitangents {
	public:
		InternalBitangents(const ObstacleModel& obstacleA, const ObstacleModel& obstacleB);
	protected:
		void e(float* destX, float* destY) override;
		void f(float* destX, float* destY) override;
		float theta() override;
	private:
		float facingBA;
	};

	class ExternalBitangents : public Bitangents {
	public:
		ExternalBitangents(const ObstacleModel& obstacleA, const ObstacleModel& obstacleB) : Bitangents(obstacleA, obstacleB) {}
	protected:
		float theta() override;
	};

}