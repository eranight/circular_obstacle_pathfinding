/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "ForestDrawerNode.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

bool HelloWorld::init()
{
    if (!Scene::init())
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto forestDrawerNode = ForestDrawerNode::create();
    if (forestDrawerNode == nullptr) {
        return false;
    }
    forestDrawerNode->setPosition(origin + visibleSize * 0.5f);
    this->addChild(forestDrawerNode, 0);

    auto closeItem = MenuItemImage::create(
        "CloseNormal.png",
        "CloseSelected.png",
        CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

    if (closeItem == nullptr ||
        closeItem->getContentSize().width <= 0 ||
        closeItem->getContentSize().height <= 0)
    {
        problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
    }
    else
    {
        float x = origin.x + visibleSize.width - closeItem->getContentSize().width / 2;
        float y = origin.y + closeItem->getContentSize().height / 2;
        closeItem->setPosition(Vec2(x, y));
    }

    auto showVerticesCheckBox = ui::CheckBox::create("CheckBox_Normal.png",
        "CheckBox_Press.png",
        "CheckBoxNode_Normal.png",
        "CheckBox_Disable.png",
        "CheckBoxNode_Disable.png");
    if (showVerticesCheckBox == nullptr) {
        return false;
    }
    else {
        float x = origin.x + visibleSize.width * 0.25 - showVerticesCheckBox->getContentSize().width / 2;
        float y = origin.y + showVerticesCheckBox->getContentSize().height / 2;
        showVerticesCheckBox->setPosition(Vec2(x, y));
        showVerticesCheckBox->addEventListener([forestDrawerNode](Ref* sender, ui::CheckBox::EventType eventType) {
            forestDrawerNode->enableVerticesDrawing(eventType == ui::CheckBox::EventType::SELECTED);
            });
        showVerticesCheckBox->setSwallowTouches(true);
    }

    auto showSurfingEdgesCheckBox = dynamic_cast<ui::CheckBox*>(showVerticesCheckBox->clone());
    if (showSurfingEdgesCheckBox == nullptr) {
        return false;
    }
    else {
        float x = origin.x + visibleSize.width * 0.31 - showSurfingEdgesCheckBox->getContentSize().width / 2;
        float y = origin.y + showSurfingEdgesCheckBox->getContentSize().height / 2;
        showSurfingEdgesCheckBox->setPosition(Vec2(x, y));
        showSurfingEdgesCheckBox->addEventListener([forestDrawerNode](Ref* sender, ui::CheckBox::EventType eventType) {
            forestDrawerNode->enableSurfingEdgesDrawing(eventType == ui::CheckBox::EventType::SELECTED);
            });
    }

    auto showHuggingEdgesCheckBox = dynamic_cast<ui::CheckBox*>(showVerticesCheckBox->clone());
    if (showHuggingEdgesCheckBox == nullptr) {
        return false;
    }
    else {
        float x = origin.x + visibleSize.width * 0.37 - showHuggingEdgesCheckBox->getContentSize().width / 2;
        float y = origin.y + showHuggingEdgesCheckBox->getContentSize().height / 2;
        showHuggingEdgesCheckBox->setPosition(Vec2(x, y));
        showHuggingEdgesCheckBox->addEventListener([forestDrawerNode](Ref* sender, ui::CheckBox::EventType eventType) {
            forestDrawerNode->enableHuggingEdgesDrawing(eventType == ui::CheckBox::EventType::SELECTED);
            });
    }

    auto additionalWidthSlider = ui::Slider::create();
    if (additionalWidthSlider == nullptr) {
        return false;
    }
    else {
        additionalWidthSlider->loadBarTexture("Slider_Back.png");
        additionalWidthSlider->loadSlidBallTextures("SliderNode_Normal.png", "SliderNode_Press.png", "SliderNode_Disable.png");
        additionalWidthSlider->loadProgressBarTexture("Slider_PressBar.png");

        float x = origin.x + visibleSize.width * 0.63 - additionalWidthSlider->getContentSize().width / 2;
        float y = origin.y + showHuggingEdgesCheckBox->getContentSize().height / 2;
        additionalWidthSlider->setPosition(Vec2(x, y));
        additionalWidthSlider->addEventListener([forestDrawerNode](Ref* sender, ui::Slider::EventType eventType) {
            if (eventType == ui::Slider::EventType::ON_PERCENTAGE_CHANGED) {
                forestDrawerNode->setAddtitionalWidth(dynamic_cast<ui::Slider*>(sender)->getPercent());
            }
            });
    }

    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    this->addChild(showVerticesCheckBox, 1);
    this->addChild(showSurfingEdgesCheckBox, 1);
    this->addChild(showHuggingEdgesCheckBox, 1);
    this->addChild(additionalWidthSlider, 1);

    /*auto mouseListener = EventListenerMouse::create();
    mouseListener->onMouseDown = [forestDrawerNode](EventMouse * event) {
        Vec2 nextLocation = forestDrawerNode->convertToNodeSpace(event->getLocationInView());
        if (event->getMouseButton() == EventMouse::MouseButton::BUTTON_LEFT) {
            forestDrawerNode->setStartPoint(nextLocation);
        }
        else if (event->getMouseButton() == EventMouse::MouseButton::BUTTON_RIGHT) {
            forestDrawerNode->setFinishPoint(nextLocation);
        }
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(mouseListener, forestDrawerNode);*/

    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->setSwallowTouches(true);
    touchListener->onTouchBegan = [forestDrawerNode](Touch* touch, Event* event) {
        Vec2 nextLocation = forestDrawerNode->convertToNodeSpace(touch->getLocation());
        forestDrawerNode->setStartPoint(nextLocation);
        return true;
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, forestDrawerNode);

    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();
}
